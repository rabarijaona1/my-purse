package org.example.mypurse.infra.spring.rest.controller;

import org.example.mypurse.domain.model.Money;
import org.example.mypurse.domain.WalletHandler;
import org.example.mypurse.infra.spring.rest.resources.RestMoney;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import static org.example.mypurse.infra.spring.rest.resources.RestMoney.toResource;

@RestController
@RequestMapping("/wallet")
public class WalletController {
	private final WalletHandler walletHandler;

	@Autowired
	public WalletController(WalletHandler walletHandler) {
		this.walletHandler = walletHandler;
	}

	@GetMapping("/balance/{accountId}")
	ResponseEntity<RestMoney> getBalance(@PathVariable String accountId) {
		Money balance = walletHandler.getBalance(accountId);
		return ResponseEntity.ok(toResource(balance));
	}

	@PostMapping("/deposit/{accountId}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	void deposit(@PathVariable String accountId, @RequestBody RestMoney moneyToDepose) {
		walletHandler.deposit(moneyToDepose.toDomain(), accountId);
	}

	@PostMapping("/withdraw/{accountId}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	void withdraw(@PathVariable String accountId, @RequestBody @Validated RestMoney moneyToWithdraw) {
		walletHandler.withdraw(moneyToWithdraw.toDomain(), accountId);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
			MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}
}
