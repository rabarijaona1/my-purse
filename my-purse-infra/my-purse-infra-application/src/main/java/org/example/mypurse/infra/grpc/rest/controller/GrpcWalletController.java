package org.example.mypurse.infra.grpc.rest.controller;

import org.example.mypurse.domain.model.Money;
import org.example.mypurse.domain.WalletHandler;
import org.example.mypurse.infra.grpc.rest.resources.AccountRequest;
import org.example.mypurse.infra.grpc.rest.resources.MoneyResponse;
import org.example.mypurse.infra.grpc.rest.resources.WalletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;

@RestController
@RequestMapping("/grpc/wallet")
public class GrpcWalletController {
	private final WalletHandler walletHandler;

	@Autowired
	public GrpcWalletController(WalletHandler walletHandler) {
		this.walletHandler = walletHandler;
	}

	@GetMapping("/balance/{accountId}")
	MoneyResponse getBalance(AccountRequest accountRequest) {
		Money balance = walletHandler.getBalance(accountRequest.getAccountId());
		return toMoneyResponse(balance);
	}

	private MoneyResponse toMoneyResponse(Money balance) {
		return MoneyResponse.newBuilder()
				.setAmount(balance.getAmount().intValue())
				.setCurrency("EUR").build();
	}

	@PostMapping("/deposit")
	void deposit(WalletRequest walletRequest) {
		walletHandler.deposit(toDomain(walletRequest.getAmount()), walletRequest.getAccountId());
	}

	private Money toDomain(long amount) {
		return new Money(BigInteger.valueOf(amount));
	}

	@PostMapping("/withdraw")
	void withdraw(WalletRequest walletRequest) {
		walletHandler.withdraw(toDomain(walletRequest.getAmount()), walletRequest.getAccountId());
	}
}
