// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: resources/Wallet.proto

package org.example.mypurse.infra.grpc.rest.resources;

public interface MoneyResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:org.example.mypurse.infra.grpc.rest.resources.MoneyResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>int64 amount = 1;</code>
   * @return The amount.
   */
  long getAmount();

  /**
   * <code>string currency = 2;</code>
   * @return The currency.
   */
  java.lang.String getCurrency();
  /**
   * <code>string currency = 2;</code>
   * @return The bytes for currency.
   */
  com.google.protobuf.ByteString
      getCurrencyBytes();
}
