package org.example.mypurse.infra.repository;

import org.example.mypurse.domain.model.Money;
import org.example.mypurse.domain.model.Wallet;
import org.example.mypurse.domain.spi.Wallets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.Optional;

@Repository
@Profile("h2Wallets")
public class H2Wallets implements Wallets {
	private WalletRepository walletRepository;

	@Autowired
	public H2Wallets(WalletRepository walletRepository) {
		this.walletRepository = walletRepository;
	}

	@Override
	public Wallet save(Wallet wallet) {
		JpaWallet jpaWallet = toJpa(wallet);
		return toDomain(walletRepository.save(jpaWallet));
	}

	private Wallet toDomain(JpaWallet jpaWallet) {
		return new Wallet(jpaWallet.getAccountId(), new Money(BigInteger.valueOf(jpaWallet.getAmount())));
	}

	private JpaWallet toJpa(Wallet wallet) {
		JpaWallet jpaWallet = walletRepository.findJpaWalletByAccountId(wallet.getAccountId());
		if (jpaWallet == null) {
			jpaWallet = new JpaWallet(wallet.getAccountId(), wallet.getBalance().getAmount().longValue());
		}
		jpaWallet.setAccountId(wallet.getAccountId());
		jpaWallet.setAmount(wallet.getBalance().getAmount().longValue());
		return jpaWallet;
	}

	@Override
	public Optional<Wallet> find(String accountId) {
		JpaWallet jpaWallet = walletRepository.findJpaWalletByAccountId(accountId);
		if (jpaWallet == null) {
			return Optional.empty();
		}
		return Optional.of(toDomain(jpaWallet));
	}
}
