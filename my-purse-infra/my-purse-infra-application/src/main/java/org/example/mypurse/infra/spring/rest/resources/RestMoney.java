package org.example.mypurse.infra.spring.rest.resources;

import org.example.mypurse.domain.model.Money;
import org.springframework.lang.NonNull;

import java.math.BigInteger;

public class RestMoney {

	@NonNull
	private BigInteger amount;

	@NonNull
	private String currency;

	public RestMoney(BigInteger amount) {
		this.amount = amount;
		this.currency = "EUR";
	}

	public BigInteger getAmount() {
		return amount;
	}

	public String getCurrency() {
		return currency;
	}

	public RestMoney() {
	}

	public static RestMoney toResource(Money money) {
		return new RestMoney(money.getAmount());
	}

	public Money toDomain() {
		return new Money(amount);
	}
}
