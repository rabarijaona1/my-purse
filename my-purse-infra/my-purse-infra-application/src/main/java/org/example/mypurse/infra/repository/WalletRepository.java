package org.example.mypurse.infra.repository;

import org.springframework.data.repository.CrudRepository;

public interface WalletRepository extends CrudRepository<JpaWallet, Long> {
	JpaWallet findJpaWalletByAccountId(String accountId);
}
