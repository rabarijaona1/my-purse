package org.example.mypurse.infra.configurations;

import org.example.mypurse.archihex.DomainService;
import org.example.mypurse.domain.WalletHandler;
import org.example.mypurse.domain.stubs.HardCodedFeesSupplier;
import org.example.mypurse.domain.stubs.InMemoryWallets;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan(
		basePackageClasses = WalletHandler.class,
		includeFilters = @ComponentScan.Filter(value = { DomainService.class }))
public class DomainConfiguration {

	@Profile("inmemoryWallet")
	@Bean
	public InMemoryWallets inMemoryWallets() {
		return new InMemoryWallets();
	}

	@Bean
	@Profile("feeSupplierStub")
	public HardCodedFeesSupplier hardCodedFeesSupplier() {
		return new HardCodedFeesSupplier();
	}

}
