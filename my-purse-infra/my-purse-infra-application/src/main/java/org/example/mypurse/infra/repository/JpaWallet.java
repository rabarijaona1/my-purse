package org.example.mypurse.infra.repository;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "WALLETS")
public class JpaWallet {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String accountId;

	private Long amount;

	protected JpaWallet() {
	}

	public JpaWallet(String accountId, Long amount) {
		this.accountId = accountId;
		this.amount = amount;
	}

	public String getAccountId() {
		return accountId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}
}