package org.example.mypurse.domain.api;

import org.example.mypurse.domain.model.Money;

public interface HandleWallet {

	Money getBalance(String accountId);

	boolean withdraw(Money money, String accountId);

	boolean deposit(Money money, String accountId);

}
