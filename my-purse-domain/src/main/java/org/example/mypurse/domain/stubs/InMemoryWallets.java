package org.example.mypurse.domain.stubs;

import org.example.mypurse.archihex.Stub;
import org.example.mypurse.domain.model.Wallet;
import org.example.mypurse.domain.spi.Wallets;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Stub
public class InMemoryWallets implements Wallets {
	private final Map<String, Wallet> walletsRepository = new ConcurrentHashMap<>();

	@Override
	public Wallet save(Wallet wallet) {
		return walletsRepository.put(wallet.getAccountId(), wallet);
	}

	@Override
	public Optional<Wallet> find(String accountId) {
		return Optional.ofNullable(walletsRepository.get(accountId));
	}
}
