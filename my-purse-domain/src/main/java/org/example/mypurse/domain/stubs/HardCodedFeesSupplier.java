package org.example.mypurse.domain.stubs;

import org.example.mypurse.archihex.Stub;
import org.example.mypurse.domain.model.Fee;
import org.example.mypurse.domain.model.Money;
import org.example.mypurse.domain.spi.RetrieveFees;

@Stub
public class HardCodedFeesSupplier implements RetrieveFees {
	public Fee currentFee = new Fee(Money.of(0));

	@Override
	public Fee retrieveFee(String name) {
		return currentFee;
	}
}
