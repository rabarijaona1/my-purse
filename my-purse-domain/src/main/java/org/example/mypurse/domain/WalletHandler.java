package org.example.mypurse.domain;

import org.example.mypurse.archihex.DomainService;
import org.example.mypurse.domain.api.HandleWallet;
import org.example.mypurse.domain.model.Fee;
import org.example.mypurse.domain.model.Money;
import org.example.mypurse.domain.model.Wallet;
import org.example.mypurse.domain.spi.RetrieveFees;
import org.example.mypurse.domain.spi.Wallets;

import java.util.Optional;

import static org.example.mypurse.domain.model.Money.add;

@DomainService
public class WalletHandler implements HandleWallet {
	private Wallets wallets;

	private RetrieveFees retrieveFees;

	public WalletHandler(Wallets wallets, RetrieveFees retrieveFees) {
		this.wallets = wallets;
		this.retrieveFees = retrieveFees;
	}

	@Override
	public Money getBalance(String accountId) {
		Optional<Wallet> wallet = wallets.find(accountId);
		if (wallet.isPresent()) {
			return wallet.get().getBalance();
		}
		return Money.of(-1);
	}

	@Override
	public boolean withdraw(Money withdrawAmount, String accountId) {
		Optional<Wallet> wallet = wallets.find(accountId);

		if (!wallet.isPresent()) {
			return false;
		}

		if (withdrawAmount.isGreaterThan(wallet.get().getBalance())) {
			return false;
		}

		Wallet currentWallet = wallet.get();
		Money newBalance = currentWallet.getBalance().minus(withdrawAmount);
		Wallet newWallet = Wallet.builder(currentWallet).withBalance(newBalance).build();
		wallets.save(newWallet);

		return true;
	}

	@Override
	public boolean deposit(Money depositAmount, String accountId) {
		Optional<Wallet> wallet = wallets.find(accountId);
		if (!wallet.isPresent()) {
			wallets.save(new Wallet(accountId, depositAmount));
			return true;
		}

		Fee fee = retrieveFees.retrieveFee(accountId);

		Wallet currentWallet = wallet.get();
		Money newBalance = add(currentWallet.getBalance(), depositAmount).minus(fee.getMoney());
		Wallet newWallet = Wallet.builder(currentWallet).withBalance(newBalance).build();
		wallets.save(newWallet);

		return true;
	}
}
