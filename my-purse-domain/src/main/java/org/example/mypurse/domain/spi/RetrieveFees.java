package org.example.mypurse.domain.spi;

import org.example.mypurse.domain.model.Fee;

public interface RetrieveFees {
	Fee retrieveFee(String name);
}
