package org.example.mypurse.domain.spi;

import org.example.mypurse.archihex.Repository;
import org.example.mypurse.domain.model.Wallet;

import java.util.Optional;

@Repository
public interface Wallets {
	Wallet save(Wallet wallet);

	Optional<Wallet> find(String accountId);

	//CRUD operation
}
