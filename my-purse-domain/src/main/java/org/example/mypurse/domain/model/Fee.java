package org.example.mypurse.domain.model;

public class Fee {

	private final Money money;

	public Fee(Money money) {
		this.money = money;
	}

	public Money getMoney() {
		return money;
	}
}
