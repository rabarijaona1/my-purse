package org.example.mypurse.domain.model;

public class Wallet {
	private final String accountId;

	private final Money balance;

	public Wallet(String accountId, Money balance) {
		this.accountId = accountId;
		this.balance = balance;
	}

	public String getAccountId() {
		return accountId;
	}

	public Money getBalance() {
		return balance;
	}

	public static WalletBuilder builder(Wallet wallet) {
		return new WalletBuilder(wallet);
	}

	public static final class WalletBuilder {

		private String accountId;

		private Money balance;

		private WalletBuilder(Wallet wallet) {
			accountId = wallet.accountId;
			balance = wallet.balance;
		}

		private WalletBuilder() {
		}

		public static WalletBuilder aWallet() {
			return new WalletBuilder();
		}

		public WalletBuilder withAccountId(String accountId) {
			this.accountId = accountId;
			return this;
		}

		public WalletBuilder withBalance(Money balance) {
			this.balance = balance;
			return this;
		}

		public Wallet build() {
			return new Wallet(accountId, balance);
		}
	}
}
