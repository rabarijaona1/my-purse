Feature: Wallet Deposit - Domain

  Scenario: A user deposits money - no policy
    Given The user account balance is $100
    When the account Holder deposits $20
    Then the account balance should be $120

  Scenario: A user deposits money - a fee is applied
    Given The user account balance is $200
    And A fee of $10 is applied
    When the account Holder deposits $20
    Then the account balance should be $210

