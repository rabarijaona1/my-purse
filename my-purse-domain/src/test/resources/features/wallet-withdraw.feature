Feature: Wallet Withdraw - Domain

  Scenario: A user withdraw money - Account has sufficient funds
    Given The user account balance is $100
    When the Account Holder requests $80
    Then the account balance should be $20 after the operation

  Scenario:  A user withdraw money - account does not have enough funds
    Given The user account balance is $100
    When the Account Holder requests $120
    And the account balance should be $100 after the operation
