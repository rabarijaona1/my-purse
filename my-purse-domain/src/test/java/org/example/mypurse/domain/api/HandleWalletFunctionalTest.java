package org.example.mypurse.domain.api;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "json:target/cucumber/wallet/cucumber-report.json" },
		features = {
				"classpath:features/wallet-deposit.feature",
				"classpath:features/wallet-withdraw.feature"
		},
		strict = true,
		glue = "org.example.mypurse.steps")
public class HandleWalletFunctionalTest {
}
