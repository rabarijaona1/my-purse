package org.example.mypurse.steps;

import cucumber.api.java8.En;
import org.example.mypurse.domain.model.Money;
import org.example.mypurse.domain.WalletHandler;
import org.example.mypurse.steps.context.WalletContext;

import static org.assertj.core.api.Assertions.assertThat;

public class WalletWithdrawStepDef implements En {

	private WalletContext walletContext;

	public WalletWithdrawStepDef(WalletContext walletContext, WalletHandler walletHandler) {
		this.walletContext = walletContext;

		When("^the Account Holder requests \\$(\\d+)$", (Integer withdrawAmount) -> {
			walletHandler.withdraw(Money.of(withdrawAmount), walletContext.currentAccountId);
		});

		Then("^the account balance should be \\$(\\d+) after the operation$", (Integer currentBalance) -> {
			assertThat(walletHandler.getBalance(walletContext.currentAccountId)).isEqualTo(Money.of(currentBalance));
		});

	}
}
