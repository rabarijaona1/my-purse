package org.example.mypurse.steps;

import cucumber.api.java8.En;
import org.example.mypurse.domain.model.Fee;
import org.example.mypurse.domain.model.Money;
import org.example.mypurse.domain.WalletHandler;
import org.example.mypurse.domain.stubs.HardCodedFeesSupplier;
import org.example.mypurse.steps.context.WalletContext;

import static org.assertj.core.api.Assertions.assertThat;

public class WalletDepositStepdefs implements En {
	private WalletContext walletContext;

	public WalletDepositStepdefs(WalletContext walletContext, WalletHandler walletHandler, HardCodedFeesSupplier hardCodedFeesSupplier) {
		this.walletContext = walletContext;

		When("^the account Holder deposits \\$(\\d+)$", (Integer depositAmount) -> {
			walletHandler.deposit(Money.of(depositAmount), walletContext.currentAccountId);
		});

		Then("^the account balance should be \\$(\\d+)$", (Integer expectedBalance) -> {
			Money currentBalance = walletHandler.getBalance(walletContext.currentAccountId);
			assertThat(Money.of(expectedBalance)).isEqualTo(currentBalance);
		});

		And("^A fee of \\$(\\d+) is applied$", (Integer fee) -> {
			hardCodedFeesSupplier.currentFee = new Fee(Money.of(fee));
		});
	}
}
