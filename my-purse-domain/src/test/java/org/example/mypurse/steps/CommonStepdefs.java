package org.example.mypurse.steps;

import cucumber.api.java8.En;
import org.example.mypurse.domain.model.Money;
import org.example.mypurse.domain.WalletHandler;
import org.example.mypurse.steps.context.WalletContext;

public class CommonStepdefs implements En {
	private WalletContext walletContext;

	public CommonStepdefs(WalletHandler walletHandler, WalletContext walletContext) {
		this.walletContext = walletContext;

		Given("^The user account balance is \\$(\\d+)$", (Integer balance) -> {
			String fabs = "Fabs";
			walletHandler.deposit(Money.of(balance), fabs);
			walletContext.currentAccountId = fabs;
		});
	}
}
