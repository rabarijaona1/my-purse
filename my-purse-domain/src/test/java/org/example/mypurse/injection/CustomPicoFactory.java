package org.example.mypurse.injection;

import cucumber.runtime.java.picocontainer.PicoFactory;
import org.example.mypurse.domain.WalletHandler;
import org.example.mypurse.domain.stubs.HardCodedFeesSupplier;
import org.example.mypurse.domain.stubs.InMemoryWallets;

public class CustomPicoFactory extends PicoFactory {
	public CustomPicoFactory() {

		addClass(InMemoryWallets.class);

		addClass(WalletHandler.class);

		addClass(HardCodedFeesSupplier.class);
	}
}
